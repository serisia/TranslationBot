﻿using Discord;
using Discord.WebSocket;
using DotNetEnv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace TranslationBot
{
    class TranslationBot
    {
        private static DiscordSocketClient client;
        private static Dictionary<string, string> transTo;

        static void Main(string[] args)
        {
            if (!File.Exists(".env"))
            {
                Logging("Please make .env file.");
                Console.ReadLine();
                Environment.Exit(0);
            }

            Env.Load();
            bool invalid = false;

            string[] needs = new string[] { "DiscordToken", "TranslateAPI" };
            foreach (string key in needs)
            {
                if (Env.GetString(key) == null)
                {
                    invalid = true;
                    Logging(string.Format("Please set [{0}] in .env file.", key));
                }
            }
            if (invalid)
            {
                Console.ReadLine();
            }
            else
            {
                transTo = Language.GetLanguageList();
                new TranslationBot().MainAsync().GetAwaiter().GetResult();
            }
        }

        public async Task MainAsync()
        {
            client = new DiscordSocketClient(new DiscordSocketConfig(){
                DefaultRetryMode = RetryMode.AlwaysRetry
            });
            client.MessageReceived += MessageReceived;
            client.Log += Log;

            await client.LoginAsync(TokenType.Bot, Env.GetString("DiscordToken"));
            await client.StartAsync();

            await Task.Delay(-1);
        }

        private async Task MessageReceived(SocketMessage message)
        {
            Console.WriteLine("{0} {1}:{2}", message.Channel.Name, message.Author.Username, message);

            if (message == null || message.Author.IsBot)
            {
                return;
            }

            // Translation
            foreach (var mention in message.MentionedUsers)
            {
                if (mention.Id == client.CurrentUser.Id)
                {
                    string[] spl = message.Content.Split('\n', 2);

                    string text = spl[1];
                    spl = spl[0].Split(' ');

                    foreach(var lang in spl)
                    {
                        if (transTo.ContainsKey(lang))
                        {
                            await translateMessage(message.Channel, message, text, transTo[lang]);
                        }
                    }
                }
            }
        }

        // ログイベント
        private Task Log(LogMessage msg)
        {
            Logging(msg.ToString(), true);
            return Task.CompletedTask;
        }

        private static void Logging(String msg, bool skipTime = false)
        {
            if(skipTime)
            {
                Console.WriteLine(msg);
            }
            else
            {
                Console.WriteLine(string.Format("{0} : {1}", DateTime.Now, msg));
            }
        }

        private async Task translateMessage(ISocketMessageChannel channel, SocketMessage msg, string text, string transTo)
        {
            try
            {
                var req = WebRequest.Create(Env.GetString("TranslateAPI") + "?text=" + text + "&source=&target=" + transTo);
                var result = req.GetResponse();
                string translated = await new StreamReader(result.GetResponseStream()).ReadToEndAsync();
                await channel.SendMessageAsync("```" + translated + "```Written by " + msg.Author.Username + " at " + msg.CreatedAt.ToString("yyyy/MM/dd HH:mm:ss (UTC)"));
            }
            catch (Exception e)
            {
                Logging(e.Message);
                Logging(e.StackTrace);
            }
        }
    }
}
