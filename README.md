# What's this?

Translation Bot for Discord in C#.

# How to use?

- Write your Bot Token in .env file.
- Write your GAS URL in .env file.
- Join your server.
- Launch Bot.
- Mention Bot user.

# Command

```
@TranslationBot ko ja
Hello.
```

first line : Mention, Translation targets  
below lines : Message

# Reference

Please visit this page for Translation GAS script.

https://qiita.com/tanabee/items/c79c5c28ba0537112922

```
function doGet(e) {
  var p = e.parameter;
  var translatedText = LanguageApp.translate(p.text, p.source, p.target);
  return ContentService.createTextOutput(translatedText);
}
```

# Language List
- af
- am
- ar
- az
- be
- bg
- bn
- bs
- ca
- co
- cs
- cy
- da
- de
- el
- en
- eo
- es
- et
- eu
- fa
- fi
- fr
- fy
- ga
- gd
- gl
- gu
- ha
- he
- hi
- hr
- ht
- hu
- hy
- id
- ig
- in
- is
- it
- iw
- ja
- ji
- jv
- jw
- ka
- kk
- km
- kn
- ko
- ku
- ky
- la
- lb
- lo
- lt
- lv
- mg
- mi
- mk
- ml
- mn
- mr
- ms
- mt
- my
- nb
- ne
- nl
- no
- ny
- pa
- pl
- ps
- pt
- ro
- ru
- sd
- si
- sk
- sl
- sm
- sn
- so
- sq
- sr
- st
- su
- sv
- sw
- ta
- te
- tg
- th
- tl
- tr
- uk
- ur
- uz
- vi
- xh
- yi
- yo
- zh
- zu
